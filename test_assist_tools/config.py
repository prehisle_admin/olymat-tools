from olymat.plugin import get_conf
from olymat.utils.dict_helper import DotDict
from olymat.utils.utils import func_run_once


class Config(DotDict):
    pass


@func_run_once
def get_g_conf() -> Config:
    return Config(get_conf())
