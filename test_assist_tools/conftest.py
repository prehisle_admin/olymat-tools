import pytest

from .config import get_g_conf
from olymat.utils.ctx_helper import make_default_ctx


@pytest.fixture(scope="session")
def cfg():
    return get_g_conf()


class CTX:  # 用于代码提示
    iid = ""
    tid = ""

@pytest.fixture(scope="session")
def ctx() -> CTX:
    yield from make_default_ctx(__file__)
