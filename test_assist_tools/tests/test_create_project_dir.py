import os
from olymat.utils.ctx_helper import get_tid
from olymat.utils.jinja2_helper import apply_jinja_to_directory
from olymat.utils.windows_helper import WindowCmdWrap2
from pathlib import Path


def test_create_olymat_prj(cfg):
    tid = get_tid()
    work_dir = cfg.CREATE_OLYMAT_PRJ.work_dir
    cmd = WindowCmdWrap2(["cmd", "/k"], shell=False)
    prj_path = os.path.join(work_dir, cfg.CREATE_OLYMAT_PRJ.project_name)
    assert not os.path.exists(prj_path), f"项目目录{prj_path}已经存在"
    os.makedirs(work_dir, exist_ok=True)
    # cmd.exec(fr"venv\scripts\deactivate", wait_str=">")
    cmd.exec(fr"set prompt=$P$G {tid}$g", wait_str=f"{tid}>")
    cmd.exec(fr"""cd /d "{work_dir}" """, wait_str=f"{tid}>")
    cmd.exec(r'''set PYTHONHOME=.''', wait_str=f"{tid}>")
    cmd.exec(r"where python", wait_str=f"{tid}>")
    cmd.exec(fr"""olymat create "{cfg.CREATE_OLYMAT_PRJ.project_name}" --init""", wait_str=f"{tid}>")
    cmd.exec(fr"", wait_str=f"{tid}>")  # 这里必须，还没找到原因


def test_init_conf_idea(cfg):
    prj_name = cfg.CREATE_OLYMAT_PRJ.project_name
    src_dir = Path(__file__).parent / "tpls"
    dst_dir = Path(cfg.CREATE_OLYMAT_PRJ.work_dir)

    dst_idea_dir = dst_dir / prj_name / ".idea"
    apply_jinja_to_directory(src_dir,
                             dst_idea_dir,
                             {
                                 "prj_name": prj_name,
                                 "prj_dir": dst_dir / prj_name,
                             })
    (dst_idea_dir / f"tpl_olymat.iml").rename(dst_idea_dir / f"{prj_name}.iml")
