import os

from olymat.utils.ctx_helper import get_iid, get_tid
from olymat.utils.windows_helper import WindowCmdWrap, WindowCmdWrap2, WindowCmdWrap3


def test_create(cfg):
    tid = get_tid()
    work_dir = cfg.CREATE_TOOL_SETS.project_root_path
    cmd = WindowCmdWrap3(["cmd", "/k"], shell=False)
    prj_path = os.path.join(work_dir, cfg.CREATE_TOOL_SETS.tool_sets_name)
    assert not os.path.exists(prj_path), f"项目目录{prj_path}已经存在"
    os.makedirs(prj_path, exist_ok=True)
    # cmd.exec(fr"venv\scripts\deactivate", wait_str=">")
    cmd.exec(fr"set prompt=$P$G {tid}$g", wait_str=f"{tid}>")
    assert "cannot" not in cmd.exec(fr"""cd /d "{prj_path}" """, wait_str=f"{tid}>", encoding="gbk")
    cmd.exec(fr"""olymat init_tpl""", wait_str=f"{tid}>")
    cmd.exec(fr"", wait_str=f"{tid}>")  # 这里必须，还没找到原因
