import os

import pytest

from olymat.utils.yaml_helper import load_yaml


@pytest.fixture(scope="session")
def CFG(CONF_NAME):
    conf = load_yaml(os.path.join(os.path.dirname(__file__), "confs",
                                  CONF_NAME if CONF_NAME.endswith(".yaml") else CONF_NAME + ".yaml"))
    return conf
