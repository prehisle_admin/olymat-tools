import pytest
from olymat.utils.ssh_helper import SshHelper


@pytest.fixture(scope="session")
def ssh(CFG):
    ssh = SshHelper(**CFG["server_ssh_conf"])
    ssh.start_chan(timeout=None)
    return ssh


def test_install_docker(ssh):
    ssh.exec_chan("curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun", until_str="~#")


def test_install_docker_compose(ssh):
    ssh.exec_chan(
        "curl -L https://get.daocloud.io/docker/compose/releases/download/v2.11.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose")
    ssh.exec_chan("chmod +x /usr/local/bin/docker-compose")


def test_conf_mirrors_ssh(ssh):
    # https://cr.console.aliyun.com/cn-hangzhou/instances/mirrors
    ssh.exec_chan('''\
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://53n57j3q.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
''')


def test_docker(ssh):
    r = ssh.exec_chan('docker --version')
    assert "Docker version" in r
    assert "build" in r


def test_docker_compose(ssh):
    r = ssh.exec_chan('docker-compose --version')
    assert "Docker Compose version" in r
